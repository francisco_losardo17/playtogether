﻿using NetworkCommsDotNet.Connections;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

namespace PlayTogether
{

    

    public partial class Form1 : Form
    {

        public Form1()
        {

            InitializeComponent();
            populateDevices();
               
            
            FormClosed += new FormClosedEventHandler((obj,evclosed) => FFMpegRecorder.StopRecording());
            Application.ApplicationExit += new EventHandler((obj, evcolsed) => TerminateServer(null, null));
            Application.ApplicationExit += new EventHandler((obj, evcolsed) => FFMpegRecorder.StopRecording());
            ServerIpTextBox.Text = "127.0.0.1:" + PlayTogether.port;
        }
        
        public void RenderConnect()
        {
            if (PlayTogether.client.myTcpConnection.ConnectionAlive())
            {
                ConnectedPanel.BackColor = Color.Green;
                ConnectedLabel.Text = "Connected!";
            }
            else
            {
                ConnectedPanel.BackColor = Color.Red;
                ConnectedLabel.Text = "Not Connected";
            }

        }

        private void TerminateServer(object sender, FormClosedEventArgs e)
        {
            if (PlayTogether.isServer)
            {
                PlayTogether.server.DisconnectSimController();
                FFMpegRecorder.StopRecording();
                Connection.StopListening();
            }
        }

        private void populateDevices()
        {
            PlayTogether.client.LoadDevices();
            foreach(var dev in PlayTogether.client.devices)
            {
                try
                {
                    ControllerSelectComboBox.Items.Add(new ComboboxItem() { Text = dev.InstanceName , Value = dev});
                }catch(Exception e)
                {

                }
            }
        }
        private void ControllerComboBoxChanged(object sender, EventArgs e)
        {
            var indexSelected = ((ComboBox)sender).SelectedIndex;
            PlayTogether.client.deviceSelected = PlayTogether.client.devices[indexSelected];
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
        private void JoinServerButtonClicked(object sender, EventArgs e)
        {
            string serverIp = ServerIpTextBox.Text.Split(':')[0];
            string portText = ServerIpTextBox.Text.Split(':')[1];
            int port = PlayTogether.port;
            if(portText != null && portText != "")
            {
                port = int.Parse(portText);
            }

            PlayTogether.client.JoinServer(serverIp, port);
            PlayTogether.client.PlayStream();
        }
        private void HostButtonClicked(object sender, EventArgs e)
        {
            PlayTogether.server.StartServer();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void elementHost1_ChildChanged(object sender, ChildChangedEventArgs e)
        {

        }

        private void elementHost1_ChildChanged_1(object sender, ChildChangedEventArgs e)
        {

        }

        private void panel4_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void ReconnectVideoBtn_Click(object sender, EventArgs e)
        {
            PlayTogether.client.PlayStream();
        }
    }

    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
