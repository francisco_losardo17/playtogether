﻿using PlayTogether.UI;

namespace PlayTogether
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ControllerSelectComboBox = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ReconnectVideoBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.ConnectedPanel = new System.Windows.Forms.Panel();
            this.ConnectedLabel = new System.Windows.Forms.Label();
            this.ConfigureControlButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ServerIpTextBox = new System.Windows.Forms.TextBox();
            this.JoinServerButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.HostServerButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.ConnectedPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ControllerSelectComboBox
            // 
            this.ControllerSelectComboBox.FormattingEnabled = true;
            this.ControllerSelectComboBox.Location = new System.Drawing.Point(34, 320);
            this.ControllerSelectComboBox.Name = "ControllerSelectComboBox";
            this.ControllerSelectComboBox.Size = new System.Drawing.Size(121, 21);
            this.ControllerSelectComboBox.TabIndex = 0;
            this.ControllerSelectComboBox.Text = "Select Controller";
            this.ControllerSelectComboBox.SelectedIndexChanged += new System.EventHandler(this.ControllerComboBoxChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ReconnectVideoBtn);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.ConnectedPanel);
            this.panel1.Controls.Add(this.ConfigureControlButton);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ServerIpTextBox);
            this.panel1.Controls.Add(this.JoinServerButton);
            this.panel1.Controls.Add(this.ControllerSelectComboBox);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(387, 426);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // ReconnectVideoBtn
            // 
            this.ReconnectVideoBtn.Location = new System.Drawing.Point(240, 253);
            this.ReconnectVideoBtn.Name = "ReconnectVideoBtn";
            this.ReconnectVideoBtn.Size = new System.Drawing.Size(129, 36);
            this.ReconnectVideoBtn.TabIndex = 8;
            this.ReconnectVideoBtn.Text = "Reconnect Video";
            this.ReconnectVideoBtn.UseVisualStyleBackColor = true;
            this.ReconnectVideoBtn.Click += new System.EventHandler(this.ReconnectVideoBtn_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "Client";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ConnectedPanel
            // 
            this.ConnectedPanel.BackColor = System.Drawing.Color.Red;
            this.ConnectedPanel.Controls.Add(this.ConnectedLabel);
            this.ConnectedPanel.Location = new System.Drawing.Point(240, 295);
            this.ConnectedPanel.Name = "ConnectedPanel";
            this.ConnectedPanel.Size = new System.Drawing.Size(129, 45);
            this.ConnectedPanel.TabIndex = 6;
            // 
            // ConnectedLabel
            // 
            this.ConnectedLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ConnectedLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ConnectedLabel.Location = new System.Drawing.Point(3, 0);
            this.ConnectedLabel.Name = "ConnectedLabel";
            this.ConnectedLabel.Size = new System.Drawing.Size(123, 45);
            this.ConnectedLabel.TabIndex = 0;
            this.ConnectedLabel.Text = "Not Connected";
            this.ConnectedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ConfigureControlButton
            // 
            this.ConfigureControlButton.Location = new System.Drawing.Point(165, 320);
            this.ConfigureControlButton.Name = "ConfigureControlButton";
            this.ConfigureControlButton.Size = new System.Drawing.Size(68, 21);
            this.ConfigureControlButton.TabIndex = 5;
            this.ConfigureControlButton.Text = "Configure";
            this.ConfigureControlButton.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 304);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Choose your Controller";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(162, 359);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Host ip + Port (xxx.xxx.xxx.xxx:8000)";
            // 
            // ServerIpTextBox
            // 
            this.ServerIpTextBox.Location = new System.Drawing.Point(162, 378);
            this.ServerIpTextBox.Name = "ServerIpTextBox";
            this.ServerIpTextBox.Size = new System.Drawing.Size(207, 20);
            this.ServerIpTextBox.TabIndex = 2;
            this.ServerIpTextBox.Text = "127.0.0.1:8000";
            // 
            // JoinServerButton
            // 
            this.JoinServerButton.Location = new System.Drawing.Point(34, 365);
            this.JoinServerButton.Name = "JoinServerButton";
            this.JoinServerButton.Size = new System.Drawing.Size(121, 40);
            this.JoinServerButton.TabIndex = 1;
            this.JoinServerButton.Text = "Join";
            this.JoinServerButton.UseVisualStyleBackColor = true;
            this.JoinServerButton.Click += new System.EventHandler(this.JoinServerButtonClicked);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.HostServerButton);
            this.panel2.Location = new System.Drawing.Point(405, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(387, 426);
            this.panel2.TabIndex = 2;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 25);
            this.label5.TabIndex = 8;
            this.label5.Text = "Host";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HostServerButton
            // 
            this.HostServerButton.Location = new System.Drawing.Point(36, 365);
            this.HostServerButton.Name = "HostServerButton";
            this.HostServerButton.Size = new System.Drawing.Size(322, 40);
            this.HostServerButton.TabIndex = 2;
            this.HostServerButton.Text = "Host";
            this.HostServerButton.UseVisualStyleBackColor = true;
            this.HostServerButton.Click += new System.EventHandler(this.HostButtonClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ConnectedPanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ControllerSelectComboBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button JoinServerButton;
        private System.Windows.Forms.Button HostServerButton;
        private System.Windows.Forms.Button ConfigureControlButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ServerIpTextBox;
        private System.Windows.Forms.Label ConnectedLabel;
        private System.Windows.Forms.Panel ConnectedPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ReconnectVideoBtn;
    }
}

