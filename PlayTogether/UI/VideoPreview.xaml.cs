﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
namespace PlayTogether.UI
{
    /// <summary>
    /// Interaction logic for VideoPreview.xaml
    /// </summary>
    public partial class VideoPreview : System.Windows.Controls.UserControl
    {
        private readonly UdpClient udp = new UdpClient("224.0.0.0",1234);
        IAsyncResult ar_ = null;

        public VideoPreview()
        {
            InitializeComponent();
            StartListening();
        }

      
        private void StartListening()
        {
            Video.Source = new Uri("udp://127.0.0.1:1234");
        }

    }
}
