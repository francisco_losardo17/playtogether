﻿using SharpDX.DirectInput;
using SimWinInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTogether
{
   
    public enum ButtonBinded
    {
        D_PAD_UP, D_PAD_DOWN, D_PAD_RIGHT, D_PAD_LEFT,
        BUTTON_A , BUTTON_B , BUTTON_X , BUTTON_Y,
        BUTTON_LB , BUTTON_RB , BUTTON_LT , BUTTON_RT,
        BUTTON_START , BUTTON_BACK 
    }


    public class ControllerBinding
    {

        public bool isButton = false;
        public bool isAxis = false;


        public int buttonIndex;
        public ButtonBinded buttonBinded;

        public ControllerBinding()
        {

        }

        public ControllerBinding(int buttonIndex, ButtonBinded buttonBinded)
        {
            isAxis = false;
            isButton = true;

            this.buttonBinded = buttonBinded;
            this.buttonIndex = buttonIndex;
        }

        public void Execute(ControllerUpdate controllerToUpdate, bool status)
        {
            switch (buttonBinded)
            {
                case ButtonBinded.BUTTON_A:
                    controllerToUpdate.BUTTON_A = status;
                    break;
                case ButtonBinded.BUTTON_B:
                    controllerToUpdate.BUTTON_B = status;
                    break;
                case ButtonBinded.BUTTON_Y:
                    controllerToUpdate.BUTTON_Y = status;
                    break;
                case ButtonBinded.BUTTON_X:
                    controllerToUpdate.BUTTON_X = status;
                    break;
                case ButtonBinded.BUTTON_LB:
                    controllerToUpdate.BUTTON_LB = status;
                    break;
                case ButtonBinded.BUTTON_RB:
                    controllerToUpdate.BUTTON_RB = status;
                    break;
                case ButtonBinded.D_PAD_DOWN:
                    controllerToUpdate.D_PAD_DOWN = status;
                    break;
                case ButtonBinded.D_PAD_LEFT:
                    controllerToUpdate.D_PAD_LEFT = status;
                    break;
                case ButtonBinded.D_PAD_RIGHT:
                    controllerToUpdate.D_PAD_RIGHT = status;
                    break;
                case ButtonBinded.D_PAD_UP:
                    controllerToUpdate.D_PAD_UP = status;
                    break;
            }
            return;
        }
    }

    class ControllerHandler
    {

        public ControllerUpdate controllerStatus = new ControllerUpdate();
        public int controllerId = 0;

        public List<ControllerBinding> bindings = new List<ControllerBinding>();

        public string leftAnalogXBind, rightAnalogXBind, leftAnalogYBind, rightAnalogYBind;


        public ControllerHandler(bool usePs4 = true)
        {
            if (usePs4)
                this.bindings = PS4BaseBindings();
        }


        public void PrintUpdate()
        {

            var fullOutput = "";

            fullOutput += "X1 :" + controllerStatus.leftAnalogX +"".PadRight(10);
            fullOutput += "\n";
            fullOutput += "X2 :" + controllerStatus.rightAnalogX + "".PadRight(10); ;
            fullOutput += "\n";
            fullOutput += "Y1 :" + controllerStatus.leftAnalogY + "".PadRight(10); ;
            fullOutput += "\n";
            fullOutput += "Y2 :" + controllerStatus.rightAnalogY + "".PadRight(10); ;
            fullOutput += "\n";
            fullOutput += "A : " + controllerStatus.BUTTON_A + "".PadRight(10) + "\n";
            fullOutput += "B : " + controllerStatus.BUTTON_B + "".PadRight(10) + "\n";
            fullOutput += "X : " + controllerStatus.BUTTON_X + "".PadRight(10) + "\n";
            fullOutput += "Y : " + controllerStatus.BUTTON_Y + "".PadRight(10) + "\n";

            Console.WriteLine(fullOutput);
        }


        internal void SetStatus(JoystickState controllerStatus)
        {
            var controllerUpdate = new ControllerUpdate();
            foreach (var bind in bindings) {
                bind.Execute(controllerUpdate, controllerStatus.Buttons[bind.buttonIndex]);
            }

            controllerUpdate.leftAnalogX = Clamp(controllerStatus.X - short.MaxValue, short.MinValue, short.MaxValue);
            controllerUpdate.leftAnalogY = Clamp(-controllerStatus.Y + short.MaxValue, short.MinValue, short.MaxValue);

            controllerUpdate.rightAnalogX = controllerStatus.Z - short.MaxValue;
            controllerUpdate.rightAnalogY = controllerStatus.RotationZ - short.MaxValue;

            controllerUpdate.leftTriggerAnalog = (byte) Clamp(controllerStatus.RotationX,0,byte.MaxValue);
            controllerUpdate.rightTriggerAnalog = (byte) Clamp(controllerStatus.RotationY, 0, byte.MaxValue);



            this.controllerStatus = controllerUpdate;
        }
        
        public void SetBinding(int buttonIndex, ButtonBinded buttonBinded)
        {
            this.bindings.Add(new ControllerBinding()
            {
                buttonBinded = buttonBinded,
                buttonIndex = buttonIndex,
                isButton = true,
                isAxis = false
            });
        }

        public List<ControllerBinding> PS4BaseBindings()
        {
            var bindings = new List<ControllerBinding>();

            bindings.Add(new ControllerBinding(0, ButtonBinded.BUTTON_X));
            bindings.Add(new ControllerBinding(1, ButtonBinded.BUTTON_A));
            bindings.Add(new ControllerBinding(2, ButtonBinded.BUTTON_B));
            bindings.Add(new ControllerBinding(3, ButtonBinded.BUTTON_Y));
            bindings.Add(new ControllerBinding(4, ButtonBinded.BUTTON_LB));
            bindings.Add(new ControllerBinding(5, ButtonBinded.BUTTON_RB));
            bindings.Add(new ControllerBinding(8, ButtonBinded.BUTTON_BACK));
            bindings.Add(new ControllerBinding(9, ButtonBinded.BUTTON_START));

            return bindings;

        }

        internal void Simulate()
        {
            var status = controllerStatus;
            
            var mySim = SimGamePad.Instance.State[controllerId];
            mySim.LeftStickX = (short) status.leftAnalogX;
            mySim.LeftStickY = (short)status.leftAnalogY;
            mySim.RightStickX = (short)status.rightAnalogX;
            mySim.RightStickY = (short)status.rightAnalogY;

            mySim.LeftTrigger = status.leftTriggerAnalog;
            mySim.RightTrigger = status.rightTriggerAnalog;

            mySim.Buttons = 0;

            if (status.BUTTON_A)
                mySim.Buttons |= GamePadControl.A;
            if (status.BUTTON_B)
                mySim.Buttons |= GamePadControl.B;
            if (status.BUTTON_X)
                mySim.Buttons |= GamePadControl.X;
            if (status.BUTTON_Y)
                mySim.Buttons |= GamePadControl.Y;
            if (status.BUTTON_LB)
                mySim.Buttons |= GamePadControl.LeftShoulder;
            if (status.BUTTON_RB)
                mySim.Buttons |= GamePadControl.RightShoulder;
            if (status.D_PAD_DOWN)
                mySim.Buttons |= GamePadControl.DPadDown;
            if (status.D_PAD_LEFT)
                mySim.Buttons |= GamePadControl.DPadLeft;
            if (status.D_PAD_RIGHT)
                mySim.Buttons |= GamePadControl.DPadRight;
            if (status.D_PAD_UP)
                mySim.Buttons |= GamePadControl.DPadUp;
            if (status.BUTTON_START)
                mySim.Buttons |= GamePadControl.Start;
            if (status.BUTTON_BACK)
                mySim.Buttons |= GamePadControl.Back;
             

            SimGamePad.Instance.Update(controllerId);
        }

        public static int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

    }
}
