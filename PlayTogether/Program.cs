﻿using PlayTogether.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PlayTogether
{
    static class Program
    {


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.ThreadException += killAll;
            AppDomain.CurrentDomain.UnhandledException += killAll;
            TaskScheduler.UnobservedTaskException += killAll;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            PlayTogether.form = new Form1();
            Application.Run(PlayTogether.form);

        }

        private static void killAll(object sender, object e)
        {
            if (PlayTogether.isServer)
            {
                PlayTogether.server.DisconnectSimController();
            }
        }
    }
}
