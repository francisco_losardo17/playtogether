﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimWinInput;
using NetworkCommsDotNet.Connections.TCP;
using NetworkCommsDotNet;
using NetworkCommsDotNet.DPSBase;
using System.Net;
using ProtoBuf.Serializers;
using SharpDX.DirectInput;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace PlayTogether
{
    class PlayTogetherClient
    {

        public TCPConnection myTcpConnection;

        public Task UpdateTask;

        public List<DeviceInstance> devices = new List<DeviceInstance>();

        public DirectInput directInput = new DirectInput();
        public DeviceInstance deviceSelected;

        public Joystick controller;
        public ControllerHandler controllerHandler;

        public Process playbackProcess; 
        public void JoinServer(string ip, int port)
        {
            SendReceiveOptions customSendReceiveOptions = new SendReceiveOptions<BinaryFormaterSerializer>();
            if(PlayTogether.connectToServer)
                myTcpConnection = TCPConnection.GetConnection(new ConnectionInfo(ip, port),customSendReceiveOptions,true);

            PlayTogether.isClient = true;

            controller = new Joystick(directInput, deviceSelected.InstanceGuid);
            controllerHandler = new ControllerHandler();

            controller.Acquire();
            UpdateTask = new Task(() =>
            {
                UpdateToServerTask();
            });


            controllerHandler.controllerId = myTcpConnection.SendReceiveObject<string, int>("Connected", "ConnectedCallback", 5000, "connected");


            UpdateTask.Start();

            PlayTogether.form.RenderConnect();
        }

        public void PlayStream()
        {
            new Task(() =>
            {
                try
                {
                    string path = Path.GetDirectoryName(
              Application.ExecutablePath);

                    var ipToStream = myTcpConnection.ConnectionInfo.RemoteEndPoint.ToString().Split(':')[0];
                    playbackProcess = new Process()
                    {
                        StartInfo =
                        {
                            FileName =  @"c:\windows\system32\cmd.exe",
                            UseShellExecute = false,
                            CreateNoWindow = false,
                            RedirectStandardOutput = false,
                            Arguments =  @"/c " +path+ @"\plugins\mplayer\mplayer -x 720 -y 480 -benchmark -v 2 rtsp://panpan:panpan@"+ipToStream+ ":"+PlayTogether.videoPort.ToString()+"/live > videolog.log"
                        },
                    };
                    playbackProcess.Start();
                    playbackProcess.PriorityClass = ProcessPriorityClass.RealTime;
                    Thread.Sleep(2000);

                }
                catch (Exception e)
                {
                    e.Message.ToArray();
                }
            }).Start();

        }

        private void SendControllerUpdate()
        {

            
            var newControllerUpdate = ReadControllerInput();
            myTcpConnection.SendObject<ControllerUpdate>("ControllerUpdate", newControllerUpdate);
        }

        private ControllerUpdate ReadControllerInput()
        {

            controller.Poll();
            JoystickState joystickState = controller.GetCurrentState();
            controllerHandler.SetStatus(joystickState);

            return controllerHandler.controllerStatus;
        }

        private void UpdateToServerTask()
        {
            while (myTcpConnection != null && myTcpConnection.ConnectionAlive() || !PlayTogether.connectToServer)
            {
                SendControllerUpdate();
                Thread.Sleep(PlayTogether.tickRate);
            }
        }

        internal void LoadDevices()
        {
            devices = this.directInput.GetDevices(DeviceClass.GameControl, DeviceEnumerationFlags.AttachedOnly).ToList();
        }

    }
}
