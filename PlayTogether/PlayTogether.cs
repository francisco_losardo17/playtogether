﻿using PlayTogether.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTogether
{
    class PlayTogether
    {

        public static bool debug = true;
        public static bool connectToServer = true;
        public static bool isServer = false;
        public static bool isClient = false;
        public static int port = 10000;

        public static int videoPort = 9000;

        public static int tickRate = 1000 / 120;

        public static PlayTogetherClient client = new PlayTogetherClient();
        public static PlayTogetherServer server = new PlayTogetherServer();

        public static FFMpegRecorder recorder = new FFMpegRecorder();
        
        public static Form1 form;
    }
}
