﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

namespace PlayTogether
{
    class FFMpegRecorder
    {
        // ffmpeg -f gdigrab -framerate 60 -i desktop -vcodec h264_nvenc -preset:v ll -f rtp -sdp_file video.sdp rtp://127.0.0.1:1234
        // ffmpeg -protocol_whitelist file,rtp,udp -i video.sdp -f mpegts -
        public static Process RecordingProcess;
        public static Process RtspServerProcess;
        public static string sdpFileName = "sdpfile.sdp";

        public void InitFFMpegRecording()
        {


            Task.Run(() =>
            {

                try
                {
                    string path = Path.GetDirectoryName(
                                  Application.ExecutablePath);

                    RtspServerProcess = new Process()
                    {
                        StartInfo =
                        {
                            FileName = @"c:\windows\system32\cmd.exe"  ,
                            Arguments = @"/c " + path + @"\plugins\rtspserver\rtsp-simple-server.exe",
                            WorkingDirectory = path + @"\plugins\rtspserver\",
                            UseShellExecute = false,
                            RedirectStandardInput = false,
                            RedirectStandardOutput = false,
                            CreateNoWindow= false
                        },
                    };

                    RtspServerProcess.Start();
                    RtspServerProcess.PriorityClass = ProcessPriorityClass.RealTime;
                    Thread.Sleep(2000);

                    RecordingProcess = new Process()
                    {
                        StartInfo =
                        {
                            FileName = @"c:\windows\system32\cmd.exe"  ,
                            Arguments = @"/c " + path + @"\plugins\ffmpeg\ffmpeg.exe -f gdigrab -framerate 30 -i desktop -c:v hevc_nvenc -preset llhq -g 60 -f rtsp -rtsp_transport tcp rtsp://panpan:panpan@127.0.0.1:"+PlayTogether.videoPort.ToString()+"/live",
                            WorkingDirectory = path,
                            UseShellExecute = false,
                            RedirectStandardInput = true,
                            RedirectStandardOutput = false,
                            CreateNoWindow=false
                        },
                    };
                    RecordingProcess.OutputDataReceived += null;

                    RecordingProcess.Start();
                    RecordingProcess.PriorityClass = ProcessPriorityClass.RealTime;
                    Thread.Sleep(5000);
                    return;
                }
                catch (Exception e)
                {
                    throw e;
                }
            });
            return;
        }

        public static void StopRecording()
        {
            if(RecordingProcess != null && !RecordingProcess.HasExited)
                RecordingProcess.StandardInput.WriteLine("q");

            if (RtspServerProcess != null && !RtspServerProcess.HasExited)
                RtspServerProcess.Kill();

        }

    }
}
