﻿using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.TCP;
using NetworkCommsDotNet.DPSBase;
using SimWinInput;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlayTogether.Server
{
    class PlayTogetherServer
    {
        public List<ControllerHandler> controllersConnected = new List<ControllerHandler>();
        public SimGamePad simPad;
        
        public void StartServer()
        {
            PlayTogether.isServer = true;
            SimGamePad.Instance.Initialize(true);
            
            AssignServerPacketHandlers();
            SendReceiveOptions customSendReceiveOptions = new SendReceiveOptions<BinaryFormaterSerializer>();
            TCPConnectionListener listener = new TCPConnectionListener(customSendReceiveOptions, ApplicationLayerProtocolStatus.Enabled, true);
            Connection.StartListening(listener,new IPEndPoint(IPAddress.Any,PlayTogether.port),false);
            
            Console.WriteLine("Listening for connections");

            new Task(() => UpdateSimControllers()).Start();
            PlayTogether.recorder.InitFFMpegRecording();
        }

        private void AssignServerPacketHandlers()
        {

            NetworkComms.AppendGlobalIncomingPacketHandler<string>("Message",
                (packetHeader, connection, incomingString) =>
                {
                    Console.WriteLine("\n  ... Incoming message from " +
                    connection.ToString() + " saying '" + incomingString + "'.");
                });

            NetworkComms.AppendGlobalConnectionEstablishHandler((obj) =>
            {
                obj.SendObject("ConnectionEstablished");
            });

            // Convert incoming data to a <Player> object and run this method when an incoming packet is received.
            NetworkComms.AppendGlobalIncomingPacketHandler<ControllerUpdate>("ControllerUpdate", (packetHeader, connection, controllerUpdate) =>
            {
                Console.SetCursorPosition(0,0);
                ControllerHandler controllerUpdated = this.controllersConnected.Find((con) => con.controllerId == controllerUpdate.controllerN);
                controllerUpdated.controllerStatus = controllerUpdate;
                controllerUpdated.PrintUpdate();
            });

            NetworkComms.AppendGlobalIncomingPacketHandler<string>("Connected", (packetHeader, connection, data) =>
            {
                InitSimController();
                connection.SendObject<int>("ConnectedCallback", 0);

            });

        }

        private void UpdateSimControllers()
        {
            while (true)
            {
                foreach (var controller in controllersConnected)
                    controller.Simulate();
                Thread.Sleep(PlayTogether.tickRate);
            }
        }

        internal void InitSimController()
        {
            SimGamePad.Instance.PlugIn(controllersConnected.Count);
            controllersConnected.Add(new ControllerHandler(true) { controllerId = controllersConnected.Count });
        }

        internal void DisconnectSimController()
        {
            SimGamePad.Instance.ShutDown();
        }



    }
}
