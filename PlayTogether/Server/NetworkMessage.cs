﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayTogether
{
    class NetworkMessage
    {

        public string Type = "UNASSIGNED";
        public object Data = null;

        public NetworkMessage(string type, object data)
        {
            this.Type = type;
            this.Data = data;
        }

        public static NetworkMessage FromJSON(string json)
        {
            throw new NotImplementedException();
        }


    }

    [Serializable]
    public class ControllerUpdate
    {

        public float leftAnalogX = 0f;
        public float leftAnalogY = 0f;

        public float rightAnalogX = 0f;
        public float rightAnalogY = 0f;

        public byte leftTriggerAnalog = 0;
        public byte rightTriggerAnalog = 0;


        public bool D_PAD_UP = false, D_PAD_DOWN = false, D_PAD_RIGHT = false, D_PAD_LEFT = false;

        public bool BUTTON_A = false, BUTTON_B = false, BUTTON_X = false, BUTTON_Y = false;

        public bool BUTTON_LB = false, BUTTON_RB = false, BUTTON_LT = false, BUTTON_RT = false;

        public bool BUTTON_START = false, BUTTON_BACK = false;

        public int controllerN = 0;

    }
}
