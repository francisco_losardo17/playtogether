﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

namespace PlayTogether
{
    class FFMpegRecorder
    {
        // ffmpeg -f gdigrab -i desktop -s 1920x1080 -framerate 30 -vcodec h264_nvenc  -f mpegts udp://224.0.0.0:1234
        public static Process RecordingProcess;

        public void InitFFMpegRecording()
        {
            Task.Run(() =>
            {

                try
                {
                    if (File.Exists("output.mp4"))
                        File.Delete("output.mp4");
                    RecordingProcess = new Process()
                    {
                        StartInfo =
                        {
                            FileName = "ffmpeg",
                            Arguments = "-f gdigrab -framerate 60 -i desktop -vcodec h264_nvenc -preset:v ll  -f mpegts udp://224.0.0.0:1234",
                            UseShellExecute = false,
                            RedirectStandardInput = true,
                            RedirectStandardOutput = true
                        },
                    };
                    RecordingProcess.Start();
                    Thread.Sleep(5000);
                    
                }
                catch (Exception e)
                {
                    throw e;
                }
            });

        }

        public static void StopRecording()
        {
            if(RecordingProcess != null)
            {
                RecordingProcess.StandardInput.WriteLine("q");
            }
        }

    }
}
