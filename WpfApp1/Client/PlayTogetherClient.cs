﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimWinInput;
using NetworkCommsDotNet.Connections.TCP;
using NetworkCommsDotNet;
using NetworkCommsDotNet.DPSBase;
using System.Net;
using SharpDX.DirectInput;
using System.Threading;

namespace PlayTogether
{
    class PlayTogetherClient
    {

        public TCPConnection myTcpConnection;

        public Task UpdateTask;

        public List<DeviceInstance> devices = new List<DeviceInstance>();

        public DirectInput directInput = new DirectInput();
        public DeviceInstance deviceSelected;

        public Joystick controller;
        public ControllerHandler controllerHandler;

        public void JoinServer(string ip, int port)
        {
            SendReceiveOptions customSendReceiveOptions = new SendReceiveOptions<BinaryFormaterSerializer>();
            if(PlayTogether.connectToServer)
                myTcpConnection = TCPConnection.GetConnection(new ConnectionInfo(ip, port),customSendReceiveOptions);

            PlayTogether.isClient = true;

            controller = new Joystick(directInput, deviceSelected.InstanceGuid);
            controllerHandler = new ControllerHandler();

            controller.Acquire();
            UpdateTask = new Task(() =>
            {
                UpdateToServerTask();
            });

            controllerHandler.controllerId = myTcpConnection.SendReceiveObject<string, int>("Connected", "ConnectedCallback", 5000, "connected");


            UpdateTask.Start();
        }

        private void SendControllerUpdate()
        {

            
            var newControllerUpdate = ReadControllerInput();
            myTcpConnection.SendObject<ControllerUpdate>("ControllerUpdate", newControllerUpdate);
        }

        private ControllerUpdate ReadControllerInput()
        {

            controller.Poll();
            JoystickState joystickState = controller.GetCurrentState();
            controllerHandler.SetStatus(joystickState);

            return controllerHandler.controllerStatus;
        }

        private void UpdateToServerTask()
        {
            while (myTcpConnection != null && myTcpConnection.ConnectionAlive() || !PlayTogether.connectToServer)
            {
                SendControllerUpdate();
                Thread.Sleep(PlayTogether.tickRate);
            }
        }

        internal void LoadDevices()
        {
            devices = this.directInput.GetDevices(DeviceClass.GameControl, DeviceEnumerationFlags.AttachedOnly).ToList();
        }

    }
}
