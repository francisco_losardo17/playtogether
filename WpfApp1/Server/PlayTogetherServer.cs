﻿using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections.TCP;
using NetworkCommsDotNet.DPSBase;
using SimWinInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PlayTogether.Server
{
    class PlayTogetherServer
    {
        public List<ControllerHandler> controllersConnected = new List<ControllerHandler>();
        public SimGamePad simPad;
        public void StartServer()
        {
            PlayTogether.isServer = true;
            SimGamePad.Instance.Initialize(true);

            AssignServerPacketHandlers();
            List<IPEndPoint> endpoints = new List<IPEndPoint>();
            endpoints.Add(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9000));
            SendReceiveOptions customSendReceiveOptions = new SendReceiveOptions<BinaryFormaterSerializer>();
            TCPConnection.StartListening(endpoints);
            new Task(() => UpdateSimControllers()).Start();
        }

        private void AssignServerPacketHandlers()
        {
            // Convert incoming data to a <Player> object and run this method when an incoming packet is received.
            NetworkComms.AppendGlobalIncomingPacketHandler<ControllerUpdate>("ControllerUpdate", (packetHeader, connection, controllerUpdate) =>
            {
                Console.SetCursorPosition(0,0);
                ControllerHandler controllerUpdated = this.controllersConnected.Find((con) => con.controllerId == controllerUpdate.controllerN);
                controllerUpdated.controllerStatus = controllerUpdate;
                controllerUpdated.PrintUpdate();
            });

            NetworkComms.AppendGlobalIncomingPacketHandler<string>("Connected", (packetHeader, connection, data) =>
            {
                InitSimController();
                connection.SendObject<int>("ConnectedCallback", 0);
            });

        }

        private void UpdateSimControllers()
        {
            while (true)
            {
                foreach (var controller in controllersConnected)
                    controller.Simulate();
                Thread.Sleep(PlayTogether.tickRate);
            }
        }

        internal void InitSimController()
        {
            SimGamePad.Instance.PlugIn(controllersConnected.Count);
            controllersConnected.Add(new ControllerHandler(true) { controllerId = controllersConnected.Count });
        }

        internal void DisconnectSimController()
        {
            SimGamePad.Instance.ShutDown();
        }



    }
}
