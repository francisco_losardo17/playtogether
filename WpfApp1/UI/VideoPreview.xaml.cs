﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlayTogether.UI
{
    /// <summary>
    /// Interaction logic for VideoPreview.xaml
    /// </summary>
    public partial class VideoPreview : UserControl
    {
        public VideoPreview()
        {
            Unosquare.FFME.Library.FFmpegDirectory = @"C:\ffmpeg\";
            InitializeComponent();
        }

        public void Play()
        {
            Media.Open(new Uri(@"D:\DOWNLOADS\try.mp4"));
            Media.Play();
        }
    }
}
