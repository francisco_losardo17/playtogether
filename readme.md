# Play Together


Play Together is an App designed to simulate virtual xinput devices on hosts and retrieve a video stream to Play Together.
App is currently under development.

## What it does
It creates the possibility of playing on a friends PC any splitscreen supported game with a virtual Xbox controller controlled from your PC.

## Main Functionality
* Host will create a session. 
* Client will select a gamepad controller from the list and connect to Host's IP. 
* Host will stream automatically it's desktop to Client through FFMPEG encoding and decoding on the HEVC_NVENC H265 codec. 
* Host will install automatically Xinput drivers and connect a virtual Xbox 360 gamepad. 
* Client will then control that virtual gamepad on host's side through a TCP connection. 

** Enabled for the time being only for PS4 controller on client side.

## Libraries Used
* NetworkCommsDotNet
* SharpDX
* SimWinGamePad

## External Apps Used
* MPlayer
* FFMpeg

